import numpy as np
import pandas as pd
import pysmiles
import networkx as nx

class DataManager(object):
    
    def __init__(self, args, useGenerators=True):
        self.args = args
        
    def load_network_data(self, data_path, data_size=None, function_node=lambda x: x, function_edge=lambda x: x):
        
        dataset = pd.read_csv(data_path)
        
        if data_size==None:
            data_size=len(dataset)

        network_dataset = pd.DataFrame()
        all_nds = []
        for i, d in dataset.iterrows():    
            try:
                nd = pysmiles.read_smiles(d['smiles'].replace("@",""))
                network_dataset = network_dataset.append(d)
                
                
                nodes = nd.nodes(data=True)
                new_nodes = []
                for i, node_feature in nodes:
                    new_nodes.append((i, function_node(node_feature)))
                nodes = new_nodes
                
                edges = nd.edges(data=True)
                new_edges = []
                for i, j, edge_feature in edges:
                    new_edges.append((i, j, function_edge(edge_feature)))
                edges = new_edges
                
                graph = nx.Graph()
                graph.graph["features"] = np.array([0], dtype=np.float)
                graph.add_nodes_from(nodes)
                graph.add_weighted_edges_from(edges, weight="features")
                
                all_nds.append(graph)
                
            except Exception as e:
                print(e)

            if i >= data_size:
                break
        network_dataset['network_data'] = all_nds
        return network_dataset