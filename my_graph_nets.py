# this was build by looking at the shortest path example at
# https://colab.research.google.com/github/deepmind/graph_nets/blob/master/graph_nets/demos/shortest_path.ipynb#scrollTo=TrGithqWUML7


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import itertools
import time

from graph_nets import graphs
from graph_nets import utils_np
from graph_nets import utils_tf
from demos import models
import networkx as nx
import numpy as np
import tensorflow as tf
import sys
from sklearn.metrics import mean_squared_error,mean_absolute_error
import threading
from multiprocessing import Queue
import time


def create_placeholders(dataset, batch_size):
    
    graph_batch = dataset['network_data'].iloc[0:batch_size]    
    y_batch = np.zeros(batch_size)
    input_ph = utils_tf.placeholders_from_networkxs(
        graph_batch, force_dynamic_num_graphs=True)
    target_ph = tf.compat.v1.placeholder(tf.float32, shape=(len(y_batch)), name="target")

    return input_ph, target_ph

def create_loss_ops(target_op, output_ops):

    batch_size = target_op.shape[0]

    loss_ops = tf.losses.mean_squared_error(target_op, output_ops)
    return loss_ops

class GraphNet:

    def __init__(self, args, dataset):
        self.args = args

        self.sess = tf.compat.v1.Session()
        self.reset_model(dataset)

    def reset_model(self, dataset):
        tf.compat.v1.reset_default_graph()

        rand = np.random.RandomState(seed=2)

        # Model parameters.
        # Number of processing (message-passing) steps.
        num_processing_steps_tr = 1
        num_processing_steps_ge = 1

        self.input_ph, self.target_ph = create_placeholders(dataset, self.args.batch_size)

        model = models.EncodeProcessDecode(edge_output_size=0, node_output_size=0,global_output_size=1)
        
        output_ops_tr = model(self.input_ph, num_processing_steps_tr)
        output_ops_ge = model(self.input_ph, num_processing_steps_ge)

        self.output_ops_tr = tf.squeeze(output_ops_tr[-1].globals)
        self.output_ops_ge = tf.squeeze(output_ops_ge[-1].globals)

        self.loss_op_tr = create_loss_ops(self.target_ph, self.output_ops_tr)
        self.loss_op_ge = create_loss_ops(self.target_ph, self.output_ops_ge)

        # Optimizer.
        optimizer = tf.train.AdamOptimizer(self.args.lr)
        self.step_op = optimizer.minimize(self.loss_op_tr)

        # reset session
        try:
          self.sess.close()
        except NameError:
          pass
        self.sess = tf.compat.v1.Session()
        self.sess.run(tf.global_variables_initializer())

    def fit(self,dataset_train, dataset_val, target_name):
        last_iteration = 0
        logged_iterations = []
        losses_tr = []
        corrects_tr = []
        solveds_tr = []
        losses_ge = []
        corrects_ge = []
        solveds_ge = []

        # run training loop

        # How much time between logging and printing the current results.
        log_every_seconds = 20

        print("# (iteration number), T (elapsed seconds), "
          "Ltr (training loss), Lge (test/generalization loss), "
          "Ctr (training fraction nodes/edges labeled correctly), "
          "Str (training fraction examples solved correctly), "
          "Cge (test/generalization fraction nodes/edges labeled correctly), "
          "Sge (test/generalization fraction examples solved correctly)")
        print("")
        start_time = time.time()
        last_log_time = start_time

        summary = {}
        summary["train_loss"] = []
        summary["train_mean_absolute_error"] = []
        summary["val_loss"] = []
        summary["val_mean_absolute_error"] = []

        for epoch in range(self.args.num_epochs):
            
            losses = []
            losses_abs = []

            the_time = time.time()
            
            #### TRAINING ####
            for iteration in range((dataset_train.shape[0]+self.args.batch_size-1)//self.args.batch_size):
                last_iteration = iteration

                x = dataset_train['network_data'][iteration*self.args.batch_size:(iteration+1)*self.args.batch_size]
                y = dataset_train[target_name][iteration*self.args.batch_size:(iteration+1)*self.args.batch_size]
                try:
                    train_values = self.sess.run({
                      "step": self.step_op,
                      "target": self.target_ph,
                      "loss": self.loss_op_tr,
                      "outputs": self.output_ops_tr
                    }, feed_dict={self.input_ph: utils_np.networkxs_to_graphs_tuple(x), self.target_ph: y})
                except:
                    pass

                elapsed_since_last_log = the_time - last_log_time

                losses.append(train_values['loss'])
                losses_abs.append(np.abs(train_values['target']-train_values['outputs']))

                elapsed = time.time() - start_time

                print("TRAIN # {:05d}, T {:.1f}, MSE {:.4f}, MAE {:.4f}".format(
                              iteration, elapsed, np.mean(losses), np.mean(losses_abs),
                            ), end="\r")
            
            summary["train_loss"].append(np.mean(losses))
            summary["train_mean_absolute_error"].append(np.mean(losses_abs))
            training_string = "TRAIN # {:05d}, T {:.1f}, MSE {:.4f}, MAE {:.4f}".format(
                              iteration, elapsed, np.mean(losses), np.mean(losses_abs),
                            )
            losses = []
            losses_abs = []
            
            #### VALIDATION ####
            for iteration in range((dataset_val.shape[0]+self.args.batch_size-1)//self.args.batch_size):
                last_iteration = iteration

                x = dataset_val['network_data'][iteration*self.args.batch_size:(iteration+1)*self.args.batch_size]
                y = dataset_val[target_name][iteration*self.args.batch_size:(iteration+1)*self.args.batch_size]

                try:
                    train_values = self.sess.run({
                      "loss": self.loss_op_tr,
                      "target": self.target_ph,
                      "outputs": self.output_ops_tr
                    }, feed_dict={self.input_ph: utils_np.networkxs_to_graphs_tuple(x), self.target_ph: y})
                except:
                    pass

                losses.append(train_values['loss'])
                losses_abs.append(np.abs(train_values['target']-train_values['outputs']))

                elapsed = time.time() - start_time

                print(training_string + " | VAL # {:05d}, T {:.1f}, MSE {:.4f}, MAE {:.4f}".format(
                              iteration, elapsed, np.mean(losses), np.mean(losses_abs),
                            ), end="\r")

            summary["val_loss"].append(np.mean(losses))
            summary["val_mean_absolute_error"].append(np.mean(losses_abs))
            print(training_string + " | VAL # {:05d}, T {:.1f}, MSE {:.4f}, MAE {:.4f}".format(
                              iteration, elapsed, np.mean(losses), np.mean(losses_abs)))



        return summary

    def predict(self,dataset,target_name):

        values_ge = np.array([])

        for iteration in range((dataset.shape[0]+self.args.batch_size-1)//self.args.batch_size):
            
            x = dataset['network_data'][iteration*self.args.batch_size:(iteration+1)*self.args.batch_size]
            y = dataset[target_name][iteration*self.args.batch_size:(iteration+1)*self.args.batch_size]
            test_values = self.sess.run({
                "target": self.target_ph,
                "loss": self.loss_op_ge,
                "outputs": self.output_ops_ge
            }, feed_dict={self.input_ph: utils_np.networkxs_to_graphs_tuple(x), self.target_ph: y})

            values_ge = np.append(values_ge,test_values["outputs"])

        return values_ge[0:dataset.shape[0]]






